import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Message } from '../../models/message';

@Component({
  selector: 'message-item',
  templateUrl: './message-item.component.html',
  styleUrls: ['./message-item.component.scss']
})
export class MessageItemComponent implements OnInit {

  @Input('message')
  private message: Message;

  @Output() actionResponse: EventEmitter<string> = new EventEmitter<string>();

  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    //this.disableSuggestions = false;
  }

  getResponse(data) {
    this.actionResponse.emit(data);
  }

}
