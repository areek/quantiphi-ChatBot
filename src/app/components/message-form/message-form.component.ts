import { Component, OnInit, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Message } from '../../models/message';
import { ChatService } from '../../services/chat-service.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'message-form',
  templateUrl: './message-form.component.html',
  styleUrls: ['./message-form.component.scss']
})
export class MessageFormComponent implements OnInit, OnChanges {

  sessionID: number;
  jwt: any;
  location: string;
  soft_skill: string;
  profile: string;
  skill: string;
  skillCounter: number;
  skills: string[];
  loopcontext: Boolean;
  disableChat: boolean = false;
  botTyping: boolean = false;
  @Input('message')
  private message : Message;

  @Input('messages')
  private messages : Message[];

  @Input('sessionId')
  private sessionId : number;

  @Input() actionResponseData: string = null;

  @Output() jsonResponse: EventEmitter<any> = new EventEmitter<any>();

  @Output() hideChat: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private chatService: ChatService, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.sessionID = this.sessionId || new Date().getTime();
    //this.sendMessage('welcome');
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.actionResponseData && this.actionResponseData != ''){
      this.message = new Message(this.actionResponseData, 'user', new Date());
      this.sendMessage(undefined);
      this.actionResponseData = null;
    }
  }

  // public sendMessage(initMsg): void {
  //   let that = this;
  //   if (!initMsg) {
  //     this.message.timestamp = new Date();
  //     this.messages.push(this.message);
  //     console.log('this======', this.message, initMsg)
  //   }
  //   this.chatService.getToken().subscribe(res => {
  //     this.jwt = res.json();
  //     this.chatService.getResponse(initMsg ? initMsg : that.message.content, this.jwt.token).subscribe(res => {
  //     // this.sendMessage();
  //     // this.actionResponseData = null;
  //   }
  // }

  public sendMessage(initMsg): void {
    if( !initMsg && (!this.message.content || this.message.content.length < 1 )) {  return; }   
    if(this.message.avatar == 'user' && this.message.content) {
      this.disableActionButtons();
    } 
    let that = this;
    if (!initMsg) {
      this.message.timestamp = new Date();
      this.messages.push(this.message);
      console.log('this======', this.message, initMsg)
    }
    this.jsonResponse.emit(null);
    let message = this.message.content;
    // this.chatService.getToken().subscribe(res => {
    //   this.jwt = res.json();
      this.chatService.sendMessageAsync(message).subscribe(res => {
        this.jsonResponse.emit(res);
        let msgObj = res.json();
        this.botTyping = false;
        this.messages.push(
          new Message(msgObj.message, 'bot', new Date())
        )
      },
      error => {
        //this.jsonResponse.emit(error);
        console.log(error);
        this.botTyping = false;
        this.snackBar.open('Something Went Wrong', 'close', {
          duration: 3000
        });
      });
    // }, error => {
    //   this.jsonResponse.emit(error);
    //   console.log(error);
    //   this.botTyping = false;
    //   this.snackBar.open('Something Went Wrong', 'close', {
    //     duration: 3000
    //   });      
    // });
    this.message = new Message('', 'user');
    this.botTyping = true;
  }

  disableActionButtons(): void {
    let actions: HTMLCollectionOf<Element> = document.getElementsByClassName('action-suggestion');
    for(let i=0; i< actions.length; i++){
      actions[i].classList.add('disabled');
    }
  }

  getMessages(fulfillmentMessages: any[]): string[]{
    let count: number = 0;
    let messageList: string[] = [];
    if(Object.keys(fulfillmentMessages[fulfillmentMessages.length - 1])[0] == 'quickReplies'){
      count = fulfillmentMessages.length - 1;
    }
    else {
      count = fulfillmentMessages.length;
    }
    for(let i=0; i<count; i++) {
      messageList.push(fulfillmentMessages[i].text.text[0]);
    }
    return messageList;
  }

  getSuggesstions(fulfillmentMessages: any[]): string[] {
    if(Object.keys(fulfillmentMessages[fulfillmentMessages.length - 1])[0] == 'quickReplies'){
      return fulfillmentMessages[fulfillmentMessages.length - 1].quickReplies.quickReplies;
    }
    return null;
  }

}
