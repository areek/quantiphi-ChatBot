import { Component, OnInit, OnChanges, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'suggestion-button-group',
  templateUrl: './suggestion-button-group.component.html',
  styleUrls: ['./suggestion-button-group.component.scss']
})
export class SuggestionButtonGroupComponent implements OnInit, OnChanges {
  @Input() actions: any;
  @Output() actionResponse: EventEmitter<string> = new EventEmitter<string>();  
  months: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  years: string[] = [];
  selectDate: boolean = false;

  selectedMonth: string = null;
  selectedYear: string = null;

  constructor() { }

  ngOnInit() {
    this.years = this.getLastTenYears((new Date()).getFullYear());
  }

  ngOnChanges() {
    //console.log(this.disable);
  }

  sendMessage(title): void {
    this.actionResponse.emit(title);
  }

  sendDate() {
    this.actionResponse.emit(this.months[(new Date()).getMonth()] + ' ' + (new Date()).getFullYear().toString());
  }
  
  private getLastTenYears(currentYear: number): string[] {
    let years = [];
    for(let i=currentYear; i>currentYear -10; i--) {
      years.push(i.toString());
    }
    return years;
  }

  yearSelected(event) {
    if(this.selectedYear && this.selectedYear.length > 0){
      this.actionResponse.emit((this.selectedMonth && this.selectedMonth.length > 0 ? this.selectedMonth + ' ' : '') + this.selectedYear)
    }
  }

  validate(data, context) {
    if(!this.selectedMonth && !this.selectedYear){ return false }
    let currentDate = new Date();
    let selectable: boolean = false;
    switch(context) {
      case 'month':
        if(this.selectedYear && this.selectedYear == currentDate.getFullYear().toString() && data > currentDate.getMonth()){
          selectable = true;
        }
        break;
      case 'year':
        if(this.selectedMonth && this.months.indexOf(this.selectedMonth) > currentDate.getMonth() && data == currentDate.getFullYear()){
          selectable = true;
        }
        break;
      default:
        break;
    }
    return selectable;
  }

}
