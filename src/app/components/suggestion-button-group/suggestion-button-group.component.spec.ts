import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuggestionButtonGroupComponent } from './suggestion-button-group.component';

describe('SuggestionButtonGroupComponent', () => {
  let component: SuggestionButtonGroupComponent;
  let fixture: ComponentFixture<SuggestionButtonGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuggestionButtonGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuggestionButtonGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
