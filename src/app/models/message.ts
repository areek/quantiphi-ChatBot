export class Message {
  content: string;
  timestamp: Date;
  avatar: string;
  actions: string[];

  constructor(content: string, avatar: string, timestamp?: Date, actions?: string[]){
    this.content = content;
    this.timestamp = timestamp;
    this.avatar = avatar;
    this.actions = actions;
  }

}
