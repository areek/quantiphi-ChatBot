import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  apiUrl: string = 'http://35.227.36.5:5000/';
  constructor(private http: Http) { }

  sendMessageAsync(message: string): Observable<any> {
    return this.http.post(this.apiUrl, { 'message': message });
  }
}
