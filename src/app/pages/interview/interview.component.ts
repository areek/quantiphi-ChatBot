import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Message } from '../../models/message';

@Component({
  selector: 'app-interview',
  templateUrl: './interview.component.html',
  styleUrls: ['./interview.component.scss']
})
export class InterviewComponent implements OnInit {
  public message : Message;
  public messages : Message[];
  actionResponseData: string = null;
  hideChat: boolean = false;
  jsonData: any;
  constructor() {
    this.message = new Message('', 'user');
    this.messages = [ ];
  }

  ngOnInit() {
  }

  getResponse(data: string) {
    this.actionResponseData = data;
    setTimeout(() => {
      this.actionResponseData = null;
    }, 2000);
  }

  getJsonData(data: any) {
    this.jsonData = data;
  }

  hideChatInput(data: boolean) {
    this.hideChat = data;
  }

}
