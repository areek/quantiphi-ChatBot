import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { InterviewComponent } from './interview.component';
import { MessageListComponent, MessageFormComponent, MessageItemComponent, SuggestionButtonGroupComponent } from '../../components/index';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatIconModule} from '@angular/material/icon';
import {MatSelectModule} from '@angular/material/select';
//import { NgxJsonViewerModule } from 'ngx-json-viewer';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '', component: InterviewComponent
      }
    ]),
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSnackBarModule,
    MatIconModule,
    MatSelectModule,
    //NgxJsonViewerModule
  ],
  declarations: [
    InterviewComponent,
    MessageListComponent,
    MessageFormComponent,
    MessageItemComponent,
    SuggestionButtonGroupComponent,
    //JsonViewComponent
    ]
})
export class InterviewModule { }